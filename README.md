# README #

### What is this repository for? ###

This repository contains the basic elements to perform UML to RAML transformations.
(The transformation itself is based on [Acceleo](https://eclipse.org/acceleo/) an implementation or OMG's MTL standard.
Given the very different setup needed to run the transformations with Acceleo (from within Eclipse or using Maven) only the .mtl source code is provided.
The transformation assumes an input model adopting the provided profiles (REST and RAML); the first is mandatory, the second is optional.
The profiles are composed by three files: the .uml are the model files and you should be able to use them with most existing UML modeler; the .di and .notation files are needed if you want to graphically edit the templates using [Papyrus](https://eclipse.org/papyrus/).

### How do I get set up? ###

* Create an empty Acceleo project and copy the .mtl file in it.
* Create a UML class model and apply the REST template to it.
* (Option) apply the RAML template too
* Model your class diagram using the Resource, GET, POST, ... stereotypes. Link resources and sub resources with Path stereotyped dependencies. All REST API elements should be contained in a API stereotyped package. More APIs can be defined in the same diagram.
* Run the MTL transformation configuring it to accept your class model as input. A .raml file will be produced.